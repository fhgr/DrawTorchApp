# -*- coding: utf-8 -*-
"""
Created on Sat Mar 21 13:50:37 2020
  drawTorchAppUB.py
  Python based drawing app using a torch/lighter and a webcam.
  Move the lighter in front of the camera to use
  as a brush for drawing
  Author: Udo Birk
  Date:   2020-04-05
  (c)2020 FH Graubünden, Chur

@author: birkudo
"""
__author__      = "Udo Birk"
__copyright__   = "Copyright 2020, FH Graubünden, Switzerland"

import cv2
import numpy as np

DEVICE = 0;
FLIP_FRAME = True;
THRESH = 0.8;

WINDOW_NAME = "Draw App Udo Birk | Press ESC to quit."

# draw colors
COLOR_B = np.uint8(255)
COLOR_G = np.uint8(255)
COLOR_R = np.uint8(255)

def main():
    global FLIP_FRAME;
    cap = cv2.VideoCapture(DEVICE);

    ret, frame = cap.read();

    gray = cv2.cvtColor(frame.astype(np.float32)/255, cv2.COLOR_BGR2GRAY);

    dst = (frame*0).astype(np.float32);
    color = (frame*0);
    
    cv2.namedWindow(WINDOW_NAME, cv2.WINDOW_NORMAL)
    
    while(True):
        
        ret, frame = cap.read();
        
        if(FLIP_FRAME):
            frame = cv2.flip(frame,1);
    
        gray = cv2.cvtColor(frame.astype(np.float32)/255, cv2.COLOR_BGR2GRAY);
    
        mask = (gray > THRESH).astype(np.float32);
        
        #gray_masked = (gray*mask*255.0).astype(np.uint8);
        
        color = cv2.cvtColor(gray*mask,cv2.COLOR_GRAY2BGR);
        color = (255*color).astype(np.uint8);
        #gray_masked = color[:,:,0];
        #color[:,:,0] = COLOR_B*gray_masked;
        #color[:,:,1] = COLOR_G*gray_masked;
        #color[:,:,2] = COLOR_R*gray_masked;
                
        draw = (252.0*dst).astype(np.uint8);
        
        draw = np.bitwise_or(draw, color);
    
        cv2.imshow(WINDOW_NAME,draw);
        
        dst = (draw/255.0).astype(np.float32);
        
        c = cv2.waitKey(20) & 0xff;
        
        if c==27:
            break
        if c==ord('q'):
            break
        
        if c==ord('f'):
            FLIP_FRAME = not FLIP_FRAME;

    cap.release()

    cv2.destroyAllWindows()    
    
    
    
if __name__=="__main__":
    main();