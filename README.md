# DrawTorchApp

drawTorchAppUB.py

  Python based drawing app using a torch/lighter and a webcam.
  
  Move the lighter in front of the camera to use
  as a brush for drawing.
  
  Author: Udo Birk  
  Date:   2020-04-05  
  (c)2020 FH Graubünden, Chur  



![Screenshot](DrawTorchApp.png)

See the included 500kb video ([DrawTorchApp.mp4](./DrawTorchApp.mp4)) for a demo.